const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

module.exports.getAll = async (event, context) => {
    console.log('Received event:', JSON.stringify(event, null, 2));

    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    };

    var tableName = process.env.TABLE_NAME;

    try {

        if (event.headers) {
            var headerItem = "business-entity-id";
            if (event.headers[headerItem]) {
                businessEntityId = event.headers[headerItem];
            }
        }

        if (businessEntityId == null) {
            throw new Error("Informatin is not sufficient.");
        }

        var params = {
            TableName: tableName,
            FilterExpression: " #businessEntityId = :businessEntityIdd",
            ExpressionAttributeNames: {
                "#businessEntityId": "businessEntityId"
            },
            ExpressionAttributeValues: {
                ":businessEntityIdd": businessEntityId
            }
        };

        let rawOutput = await dynamo.scan(params).promise();
        body = rawOutput.Items;

    }
    catch (err) {
        statusCode = '400';
        body = err.message;
    }
    finally {
        body = JSON.stringify(body, (key, value) => {
            return (typeof value === 'bigint') ? value.toString() : value
        })
        body = JSON.parse(body);
        body = JSON.stringify(body);
    }

    console.log("Response to be returned - [body]:", body);
    console.log("Response to be returned - [statusCode]:", statusCode);
    console.log("Response to be returned - [headers]:", headers);

    return {
        statusCode,
        body,
        headers,
    };
};

module.exports.getById = async (event, context) => {
    console.log('Received event:', JSON.stringify(event, null, 2));

    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    };

    var tableName = process.env.TABLE_NAME;

    try {
        if (event.pathParameters != null) {
            let id = event.pathParameters.id;
            console.log("Path parameters: ", event.pathParameters, " Id:", id);
            var params = {
                TableName: tableName,
                KeyConditionExpression: "#itemId = :itemIdd",
                ExpressionAttributeNames: {
                    "#itemId": "id"
                },
                ExpressionAttributeValues: {
                    ":itemIdd": id
                }
            };

            let rawOutput = await dynamo.query(params).promise();
            body = rawOutput.Items[0];
        }
    }
    catch (err) {
        statusCode = '400';
        body = err.message;
    }
    finally {
        body = JSON.stringify(body, (key, value) => {
            return (typeof value === 'bigint') ? value.toString() : value
        })
        body = JSON.parse(body);
        body = JSON.stringify(body);
    }

    console.log("Response to be returned - [body]:", body);
    console.log("Response to be returned - [statusCode]:", statusCode);
    console.log("Response to be returned - [headers]:", headers);

    return {
        statusCode,
        body,
        headers,
    };
};