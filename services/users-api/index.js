const restHandler = require('libs/rest-handler');

// Global variable initializations
let restHelper = new restHandler.RestHelper();
const HttpMethod = restHandler.HttpMethod;

class UserValidator {
    apply(object) {
        
    }
}

module.exports.handle = (event, context, callback) => {

    console.log("resource:" + process.env.resource);
    switch (event.httpMethod) {
        case HttpMethod.GET:
            this.handleGet(event, context, callback);
            break;
        case HttpMethod.PUT:
            this.update(event, context, callback);
            break;
        case HttpMethod.POST:
            this.create(event, context, callback);
            break;
        default:
            console.log("Error: Unsupported Method");
    }
}

module.exports.handleGet = (event, context, callback) => {

    let dbQueryParams = restHelper.getDBQueryParams(process.env.TABLE_NAME,
        event.pathParameters, event.queryStringParameters);

    if (event.pathParameters != null && event.pathParameters.id != null) {
        restHandler.getFirst(dbQueryParams, context)
            .then(dbResult => restHandler.parseDBResult(dbResult))
            .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj)))
            .catch(e => {
                console.log("Handle Get(first) Error: " + e);
                callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.GET));
            });
    }
    else {
        restHandler.getAll(dbQueryParams, context)
            .then(dbResult => restHandler.parseDBResult(dbResult))
            .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj)))
            .catch(e => {
                console.log("Handle Get(All) Error" + e)
                callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.GET));
            });
    }
}

module.exports.create = (event, context, callback) => {
    console.log("Event Body:", event.body);
    var obj = JSON.parse(event.body);

    restHandler.preprocessRequest(obj,
        new restHandler.CREATE_OBJ_VALIDATOR(),
        new UserValidator(),
        new restHandler.UUID_POPULATOR(),
        new restHandler.TIMESTAMP_POPULATOR())
        .then(output => restHandler.putDB(output, context))
        .then(dbResult => restHandler.parseDBResult(dbResult))
        .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj, HttpMethod.POST)))
        .catch(e => {
            callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.POST));
        });

};

module.exports.update = (event, context, callback) => {
    console.log("Event Body:", event.body);
    var obj = JSON.parse(event.body);

    restHandler.preprocessRequest(obj,
        new restHandler.CREATE_OBJ_VALIDATOR(),
        new UserValidator(),
        // Any object specific validation before updating the object
        new restHandler.UUID_POPULATOR(),
        new restHandler.TIMESTAMP_POPULATOR())
        .then(output => restHandler.putDB(output, context))
        .then(dbResult => restHandler.parseDBResult(dbResult))
        .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj, HttpMethod.PUT)))
        .catch(e => {
            callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.PUT));
        });
};