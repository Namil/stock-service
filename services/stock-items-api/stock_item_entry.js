const restHandler = require('libs/rest-handler');

module.exports.getSummery = (event, context, callback) => {
    console.log('Received event:', JSON.stringify(event, null, 2));

    try {

        restHandler.populateFilterQueryFromQueryParams(event.queryStringParameters)
            .then(params => restHandler.getAll(params, context))
            .then(resultSet => restHandler.postprocessRequest(resultSet, {

                apply(stockItemEntries) {
                    let summery = [];
                    for (var i = 0; i < stockItemEntries.length; i++) {
                        let total = Number(stockItemEntries[i].quantity) * Number(stockItemEntries[i].updateDirection);

                        let summeryEntry = summery.find(element => element.stockItemReferenceId == stockItemEntries[i].stockItemReferenceId);
                        if (summeryEntry == undefined) {
                            summeryEntry = {
                                stockItemReferenceId: stockItemEntries[i].stockItemReferenceId,
                                total: total
                            }
                            summery.push(summeryEntry);
                        }
                        else {
                            summeryEntry.total = (summeryEntry.total + total);
                        }
                    }

                    console.log("Stock Sumeries" + JSON.stringify(summery));
                    return summery.length > 1 ? summery : summery[0];
                    // console.log("Stock Sumeries - Object" + JSON.stringify(object)); 
                }
            }))
            .then(dbResult => restHandler.parseDBResult(dbResult))
            .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj)))
            .catch(e => {
                console.log("Error" + e)
                callback(null, restHandler.generateErrorJsonResponseResult(e, restHandler.HttpMethod.GET));
            });
    }
    catch (err) {
        console.log("Error" + err)
        callback(null, restHandler.generateErrorJsonResponseResult(err, restHandler.HttpMethod.GET));
    }
};