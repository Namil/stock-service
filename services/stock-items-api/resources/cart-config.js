
const restHandler = require('libs/rest-handler');


class CartValidator {
    apply(object) {
        console.log("CartValidator::apply() - Busines Entity ID: ["+object.businessEntityId+"]")
        if (!object.businessEntityId) {
            throw new Error("CartValidator::apply() - No <businessEntityId> property available");
        }
    }
}

let cartConfig = {

    create: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new CartValidator()],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
                {

                    apply(object) {

                        if (object.cartItems != null) {
                            let uuidPopulator = new restHandler.UUID_POPULATOR();
                            let timestampPopulator = new restHandler.TIMESTAMP_POPULATOR();

                            object.cartItems.forEach(cartItem => {
                                uuidPopulator.apply(cartItem);
                                timestampPopulator.apply(cartItem);

                                if (cartItem.subCartItems != null) {

                                    cartItem.subCartItems.forEach(subCartItem => {
                                        uuidPopulator.apply(subCartItem);
                                        timestampPopulator.apply(subCartItem);
                                    });
                                }
                            })
                        }
                    }
                }]

        },
        postProcess: {

        }
    },
    update: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new CartValidator()
            ],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
                {
                    apply(object) {

                        if (object.cartItems != null) {

                            let uuidPopulator = new restHandler.UUID_POPULATOR();
                            let timestampPopulator = new restHandler.TIMESTAMP_POPULATOR();

                            object.cartItems.forEach(cartItem => {
                                uuidPopulator.apply(cartItem);
                                timestampPopulator.apply(cartItem);

                                if (cartItem.subCartItems != null) {

                                    cartItem.subCartItems.forEach(subCartItem => {
                                        uuidPopulator.apply(subCartItem);
                                        timestampPopulator.apply(subCartItem);
                                    });
                                }
                            })
                        }
                    }
                }]

        },
        postProcess: {

        }
    }
}

module.exports.cartConfig = cartConfig;