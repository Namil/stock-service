
const restHandler = require('libs/rest-handler');


class PaymentAdviseValidator {
    apply(object) {
        console.log("CartValidator::apply() - Busines Entity ID: ["+object.businessEntityId+"]")
        if (!object.businessEntityId) {
            throw new Error("PaymentAdviseValidator::apply() - No <businessEntityId> property available");
        }
    }
}

let paymentAdviseConfig = {

    create: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new PaymentAdviseValidator()],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
            ]

        },
        postProcess: {

        }
    },
    update: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new PaymentAdviseValidator()
            ],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
            ]

        },
        postProcess: {

        }
    }
}

module.exports.paymentAdviseConfig = paymentAdviseConfig;