
const restHandler = require('libs/rest-handler');


class StockItemValidator {
    
    apply(object) {
        if (!object.barcode) {
            throw new Error("StockItemValidator::apply() - No <barcode> property available");
        }
        if (!object.unitPrice) {
            throw new Error("StockItemValidator::apply() - No <unitPrice> property available");
        }
    }
}

let stockItemConfig = {

    create: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new StockItemValidator()],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
                {
                    apply(object) {

                        if (object.subStockItems && object.subStockItems.length > 0) {
                            let uuidPopulator = new restHandler.UUID_POPULATOR();
                            let timestampPopulator = new restHandler.TIMESTAMP_POPULATOR();
                            object.subStockItems.forEach(subStockItem => {
                                uuidPopulator.apply(subStockItem);
                                timestampPopulator.apply(subStockItem);
                            });
                        }
                    }
                }]

        },
        postProcess: {

        }
    },
    update: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new StockItemValidator()
                // Any object specific validation before updating the object
            ],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
                {
                    apply(object) {

                        if (object.subStockItems && object.subStockItems.length > 0) {

                            let uuidPopulator = new restHandler.UUID_POPULATOR();
                            let timestampPopulator = new restHandler.TIMESTAMP_POPULATOR();

                            object.subStockItems.forEach(subStockItem => {
                                uuidPopulator.apply(subStockItem);
                                timestampPopulator.apply(subStockItem);
                            });
                        }
                    }
                }]

        },
        postProcess: {

        }
    }
}

module.exports.stockItemConfig = stockItemConfig;