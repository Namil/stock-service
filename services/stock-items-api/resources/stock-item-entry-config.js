
const restHandler = require('libs/rest-handler');


class StockItemEntryValidator {
    apply(object) {
        console.log("StockItemEntryValidator::apply()");
        console.log("StockItemEntryValidator::apply()- end");
    }
}

let stockItemEntryConfig = {

    create: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new StockItemEntryValidator()],

            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
            ]

        },
        postProcess: {

        }
    },
    update: {
        preProcess: {
            validators: [
                new restHandler.CREATE_OBJ_VALIDATOR(),
                new StockItemEntryValidator()
                // Any object specific validation before updating the object

            ],
            enrichments: [
                new restHandler.UUID_POPULATOR(),
                new restHandler.TIMESTAMP_POPULATOR(),
                {
                    apply(object) {

                        let uuidPopulator = new restHandler.UUID_POPULATOR();
                        let timestampPopulator = new restHandler.TIMESTAMP_POPULATOR();

                        object.subStockItems.forEach(subStockItem => {
                            uuidPopulator.apply(subStockItem);
                            timestampPopulator.apply(subStockItem);
                        });
                    }
                }]

        },
        postProcess: {

        }
    }
}

module.exports.stockItemEntryConfig = stockItemEntryConfig;