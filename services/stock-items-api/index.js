const restHandler = require('libs/rest-handler');
const { RestConfigManager } = require('./config-manager');

// Global variable initializations
let restHelper = new restHandler.RestHelper();
const HttpMethod = restHandler.HttpMethod;

module.exports.handle = (event, context, callback) => {

    console.log("resource:" + process.env.resource);

    let restConfig = RestConfigManager.getRestConfig(process.env.resource)

    switch (event.httpMethod) {
        case HttpMethod.GET:
            this.handleGet(event, context, callback);
            break;
        case HttpMethod.PUT:
            this.update(event, context, callback, restConfig);
            break;
        case HttpMethod.POST:
            this.create(event, context, callback, restConfig);
            break;
        default:
            console.log("Error: Unsupported Method");
    }
}

module.exports.handleGet = (event, context, callback) => {

    let dbQueryParams = restHelper.getDBQueryParams(process.env.TABLE_NAME,
        event.pathParameters, event.queryStringParameters);

    if (event.pathParameters != null && event.pathParameters.id != null) {
        restHandler.getFirst(dbQueryParams, context)
            .then(dbResult => restHandler.parseDBResult(dbResult))
            .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj)))
            .catch(e => {
                callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.GET));
            });
    }
    else {
        restHandler.getAll(dbQueryParams, context)
            .then(dbResult => restHandler.parseDBResult(dbResult))
            .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj)))
            .catch(e => {
                console.log("Error" + e)
                callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.GET));
            });
    }
}

module.exports.create = (event, context, callback, restConfig) => {
    console.log("Event Body:", event.body);
    var obj = JSON.parse(event.body);

    restHandler.preprocess(obj,
        restConfig.create.preProcess.validators, restConfig.create.preProcess.enrichments
    ).then(output => restHandler.putDB(output, context))
        .then(dbResult => restHandler.parseDBResult(dbResult))
        .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj, HttpMethod.POST)))
        .catch(e => {
            callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.POST));
        });
};

module.exports.update = (event, context, callback, restConfig) => {
    console.log("Event Body:", event.body);
    var obj = JSON.parse(event.body);

    console.log("parsed object:" + obj);

    restHandler.preprocess(obj,
        restConfig.update.preProcess.validators, restConfig.update.preProcess.enrichments
    ).then(output => restHandler.putDB(output, context))
        .then(dbResult => restHandler.parseDBResult(dbResult))
        .then(resultObj => callback(null, restHandler.generateSuccessJsonResponseResult(resultObj, HttpMethod.PUT)))
        .catch(e => {
            callback(null, restHandler.generateErrorJsonResponseResult(e, HttpMethod.PUT));
        });
};