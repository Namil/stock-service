const { cartConfig } = require('./resources/cart-config');
const { paymentAdviseConfig } = require('./resources/payment-advise-config');
const { stockItemConfig } = require('./resources/stock-item-config')
const { stockItemEntryConfig } = require('./resources/stock-item-entry-config')

module.exports.RestConfigManager = {

    getRestConfig(resource) {

        switch (resource) {
            case "CART":
                return cartConfig;
            case "PAYMENT_ADVISE":
                return paymentAdviseConfig;
            case "STOCK_ITEM":
                return stockItemConfig;
            case "STOCK_ITEM_ENTRY":
                return stockItemEntryConfig;
            default:
                throw Error("No config available");
        }
    }
}