const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB.DocumentClient();

const Handlebars = require('handlebars');
const fs = require('fs');
const moment =  require("moment");
const momenttz =  require("moment-timezone");

var template = fs.readFileSync("templates/58mmReceipt.handlebars", "utf8");


const compiledTemplate = Handlebars.compile(template);

Handlebars.registerHelper('ifCondEq', function(v1, v2, options) {
  if(v1 === v2) {
    return options.fn(this);
  }
  return options.inverse(this);
});

Handlebars.registerHelper('currency', function (amount) {
    var num =  Number(amount);
var formattedNumber = num.toLocaleString('en-US', { minimumFractionDigits: 2});

return formattedNumber;

});

Handlebars.registerHelper('DateTimeFormat', function (value) {

let date = moment.utc(value);
let formatTime = momenttz(date).tz('Asia/Colombo').format("YYYY-MM-DD HH:mm:ss");

return formatTime;

});

exports.getById = async(event, context) => {

    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    };

    try {

        var tableName = "stock-dev-PAYMENT_ADVISE_V4";

        if (event.pathParameters != null) {
            let id = Number(event.pathParameters.id);
            console.log("Path parameters: ", event.pathParameters, " Id:", id);
            var params = {
                TableName: tableName,
                KeyConditionExpression: "#itemId = :itemIdd ",
                ExpressionAttributeNames: {
                    "#itemId": "id"
                },
                ExpressionAttributeValues: {
                    ":itemIdd": id
                }
            };

            let rawOutput = await dynamo.query(params).promise();
            let paymentAdvise = rawOutput.Items[0];

            tableName = "stock-dev-CART_V4";

            params = {
                TableName: tableName,
                KeyConditionExpression: "#itemId = :itemIdd ",
                ExpressionAttributeNames: {
                    "#itemId": "id"
                },
                ExpressionAttributeValues: {
                    ":itemIdd": paymentAdvise.cartId
                }
            };

            rawOutput = await dynamo.query(params).promise();
            let cart = rawOutput.Items[0];
            
            tableName = "BUSINESS_ENTITY";

            params = {
                TableName: tableName,
                KeyConditionExpression: "#itemId = :itemIdd ",
                ExpressionAttributeNames: {
                    "#itemId": "id"
                },
                ExpressionAttributeValues: {
                    ":itemIdd": ""+cart.businessEntityId
                }
            };

            rawOutput = await dynamo.query(params).promise();
            let businessEntity = rawOutput.Items[0];

            var data = {
                paymentAdvise: paymentAdvise,
                cart: cart,
                businessEntity: businessEntity
            };

            console.log("Data: "+ JSON.stringify(data));


            let output = compiledTemplate({ name: "Nils" });

            // return output;

            var template1 = fs.readFileSync("templates/58mmReceipt.handlebars", "utf8");
            // var data = { message: "Hello world!" };

            var compileTemplate = Handlebars.compile(template1);
            var finalPageHTML = compileTemplate(data);

            body = finalPageHTML;

        }
    }
    catch (err) {
        statusCode = '400';
        body = err.message;
    }
    finally {

    }

    console.log("Response to be returned - [body]:", body);
    console.log("Response to be returned - [statusCode]:", statusCode);
    console.log("Response to be returned - [headers]:", headers);

    return {
        statusCode,
        body,
        headers,
    };
}
