const AWS = require('aws-sdk');

const dynamo = new AWS.DynamoDB.DocumentClient();

/**
 * Demonstrates a simple HTTP endpoint using API Gateway. You have full
 * access to the request and response payload, including headers and
 * status code.
 *
 * To scan a DynamoDB table, make a GET request with the TableName as a
 * query string parameter. To put, update, or delete an item, make a POST,
 * PUT, or DELETE request respectively, passing in the payload to the
 * DynamoDB API as a JSON body.
 */

var CUSTOMEPOCH = 1300000000000; // artificial epoch
function generateRowId(shardId /* range 0-64 for shard/slot */ ) {
    var ts = new Date().getTime() - CUSTOMEPOCH; // limit to recent
    var randid = Math.floor(Math.random() * 512);
    ts = (ts * 64); // bit-shift << 6
    ts = ts + shardId;
    return (ts * 512) + (randid % 512);
}

exports.handler = async(event, context) => {
    console.log('Received event:', JSON.stringify(event, null, 2));

    var tableName = process.env.TABLE_NAME;
    var createdTimestamp = new Date().toISOString();
    var lastUpdatedTimestamp = createdTimestamp;
    var businessEntityId = 0;


    let body;
    let statusCode = '200';
    const headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "DELETE,GET,HEAD,OPTIONS,PATCH,POST,PUT",
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    };

    console.log("inside Lambda HTTP Method:", event.httpMethod);

    try {

        if (event.headers) {
            var headerItem = "business-entity-id";
            if (event.headers[headerItem]) {
                businessEntityId = event.headers[headerItem];
            }
        }

        // if (businessEntityId == null) {
        //     throw new Error("Informatin is not sufficient.");
        // }

        switch (event.httpMethod) {
            case 'DELETE':
                body = await dynamo.delete(JSON.parse(event.body)).promise();
                break;
            case 'GET':

                if (event.pathParameters != null) {
                    let reportId = Number(event.pathParameters.id);
                    console.log("Path parameters -  ", event.pathParameters, " Report Id:", reportId);

                    if (reportId == 1) {
                        // All entries

                        let filterExpression = "#status = :statusd  and  #businessEntityId = :businessEntityIdd "; // Initial conditions
                        let expressionAttributeNames = {
                            "#status": "status",
                            "#businessEntityId": "businessEntityId"
                        };
                        let expressionAttributeValues = {
                            ":statusd": "COMPLETE",
                            ":businessEntityIdd": businessEntityId
                        };

                        let start_date = null;
                        let end_date = null;
                        let item_name = null;

                        if (event.queryStringParameters != null) {
                            console.log("Query String parameters:", event.queryStringParameters);

                            //Handle requests with query string parameters here.

                            if (event.queryStringParameters.itemName != null) {
                                item_name = event.queryStringParameters.itemName;
                                console.log("Item Name:", item_name);

                                //Append queryString parameters to the scan query.
                                // filterExpression = filterExpression + " AND cartItems.CONTAINS(stockReferenceId , :item_name)  ";
                                // expressionAttributeValues = {
                                //     ":statusd": "COMPLETE",
                                //     ":start_date": start_date.toISOString(),
                                //     ":end_date": end_date.toISOString(),
                                //     ":item_name" : item_name,
                                //     ":businessEntityIdd": businessEntityId
                                // }
                                // expressionAttributeValues[":item_name"] = item_name;
                            }

                            if (event.queryStringParameters.createdDate != null) {
                                let createdDate = event.queryStringParameters.createdDate;
                                console.log("Created Date:", createdDate);

                                start_date = new Date(createdDate);
                                end_date = new Date(start_date.getTime() + 86400000); // + 1 day in ms


                            }

                            if (event.queryStringParameters.endDate != null) {
                                let endDatePar = event.queryStringParameters.endDate;
                                console.log("End Date:", endDatePar);
                                let temp_end_date = new Date(endDatePar);

                                if (end_date <= temp_end_date) {
                                    end_date = temp_end_date;
                                }
                            }

                            if (start_date != null) {
                                //Append queryString parameters to the scan query.
                                filterExpression = filterExpression + "AND createdTimestamp between :start_date and :end_date ";
                                expressionAttributeValues = {
                                    ":statusd": "COMPLETE",
                                    ":start_date": start_date.toISOString(),
                                    ":end_date": end_date.toISOString(),
                                    ":businessEntityIdd": businessEntityId
                                }
                            }
                        }

                        console.log("Start to retrieve data: " + tableName);

                        var params = {
                            TableName: tableName,
                            FilterExpression: filterExpression,
                            ExpressionAttributeNames: expressionAttributeNames,
                            ExpressionAttributeValues: expressionAttributeValues,
                            ProjectionExpression: 'cartItems',
                        };

                        let rawOutput = await dynamo.scan(params).promise();
                        let data = [];
                        var dataSummery = new Map();

                        for (let i = 0; i < rawOutput.Items.length; i++) {

                            for (var j = 0; j < rawOutput.Items[i].cartItems.length; j++) {

                                if (item_name != null) {
                                    if (rawOutput.Items[i].cartItems[j].stockReferenceId == item_name) {
                                        data.push(rawOutput.Items[i].cartItems[j]);
                                    }
                                }
                                else {
                                    data.push(rawOutput.Items[i].cartItems[j]);
                                }

                                var stockSummery = dataSummery.get(rawOutput.Items[i].cartItems[j].stockReferenceId);
                                if (stockSummery == null || stockSummery == undefined) {
                                    dataSummery.set(rawOutput.Items[i].cartItems[j].stockReferenceId,
                                        rawOutput.Items[i].cartItems[j].name
                                    );
                                }
                            }

                        }

                        let itemNameSelectValues = [];
                        for (let [k, v] of dataSummery) {
                            console.log("Key: " + k);
                            console.log("Value: " + v);
                            itemNameSelectValues.push({ id: k, value: v });
                        }

                        let reportData = {
                            id: 1,
                            displayName : "Full Report",
                            columnDefinitions: [
                                { id: "position", type: "string", displayName: "ID", dataReference: "id" },
                                { id: "name", type: "string", displayName: "Name", dataReference: "name" },
                                { id: "unitPrice", type: "currency", displayName: "Price", dataReference: "unitPrice" },
                                { id: "createdTimestamp", type: "date", displayName: "Created Date", dataReference: "createdTimestamp" },
                                { id: "symbol", type: "string", displayName: "Quantity", dataReference: "quantity" }
                            ],
                            filterDefinitions: [
                                { id: "createdDate", displayName: "Start Date", type: "DateRange", selectedValue: start_date != null ? start_date.toISOString() : null },
                                { id: "endDate", displayName: "End Date", type: "DateRange", selectedValue: end_date != null ? end_date.toISOString() : null },
                                {
                                    id: "itemName",
                                    displayName: "Item",
                                    type: "select",
                                    selectedValue: item_name != null ? item_name : null,
                                    options: itemNameSelectValues

                                }
                            ],
                            displayedColumns: ['position', 'name', 'unitPrice', 'createdTimestamp', 'symbol'],
                            pageSizeOptions: [5, 10, 20],
                            data: data
                        }

                        body = reportData;

                    }
                    else if (reportId == 2) {
                        // All entries summery

                        let filterExpression = "#status = :statusd  and  #businessEntityId = :businessEntityIdd "; // Initial conditions
                        let expressionAttributeNames = {
                            "#status": "status",
                            "#businessEntityId": "businessEntityId"
                        };
                        let expressionAttributeValues = {
                            ":statusd": "COMPLETE",
                            ":businessEntityIdd": businessEntityId
                        };

                        let start_date = null;
                        let end_date = null;

                        if (event.queryStringParameters != null) {
                            console.log("Query String parameters:", event.queryStringParameters);

                            //Handle requests with query string parameters here.
                            if (event.queryStringParameters.createdDate != null) {
                                let createdDate = event.queryStringParameters.createdDate;
                                console.log("Created Date:", createdDate);

                                start_date = new Date(createdDate);
                                end_date = new Date(start_date.getTime() + 86400000); // + 1 day in ms
                            }

                            if (event.queryStringParameters.endDate != null) {
                                let endDatePar = event.queryStringParameters.endDate;
                                console.log("End Date:", endDatePar);
                                let temp_end_date = new Date(endDatePar);

                                if (end_date < temp_end_date) {
                                    end_date = temp_end_date;
                                }
                            }
                            //Append queryString parameters to the scan query.
                            filterExpression = filterExpression + "AND createdTimestamp between :start_date and :end_date ";
                            expressionAttributeValues = {
                                ":statusd": "COMPLETE",
                                ":start_date": start_date.toISOString(),
                                ":end_date": end_date.toISOString(),
                                ":businessEntityIdd": businessEntityId
                            }
                        }

                        var params = {
                            TableName: tableName,
                            FilterExpression: filterExpression,
                            ExpressionAttributeNames: expressionAttributeNames,
                            ExpressionAttributeValues: expressionAttributeValues,
                            ProjectionExpression: 'cartItems',
                        };

                        let rawOutput = await dynamo.scan(params).promise();
                        let data = [];

                        var dataSummery = new Map();

                        for (let i = 0; i < rawOutput.Items.length; i++) {

                            for (var j = 0; j < rawOutput.Items[i].cartItems.length; j++) {
                                let tempObj = rawOutput.Items[i].cartItems[j]

                                let stockReferenceId = tempObj.stockReferenceId;
                                let unitPrice = tempObj.unitPrice;

                                let objectKey = "" + stockReferenceId + unitPrice;

                                var stockSummery = dataSummery.get(objectKey);
                                if (stockSummery == null || stockSummery == undefined) {
                                    stockSummery = tempObj;
                                }
                                else {
                                    stockSummery.quantity = Number(stockSummery.quantity) + Number(tempObj.quantity);
                                    //   stockSummery.quantity += tempObj.quantity;
                                }

                                dataSummery.set(objectKey, stockSummery);

                                // data.push(stockSummery);
                            }
                        }

                        for (let [k, v] of dataSummery) {
                            console.log("Key: " + k);
                            console.log("Value: " + v);
                            data.push(v);
                        }

                        let reportData = {
                            id: reportId,
                            displayName : "Sales Report",
                            columnDefinitions: [
                                { id: "position", type: "string", displayName: "ID", dataReference: "id" },
                                { id: "name", type: "string", displayName: "Name", dataReference: "name" },
                                { id: "unitPrice", type: "currency", displayName: "Price", dataReference: "unitPrice" },
                                // { id: "createdTimestamp", type: "date", displayName: "Created Date", dataReference: "createdTimestamp" },
                                { id: "symbol", type: "string", displayName: "Quantity", dataReference: "quantity" }
                            ],
                            filterDefinitions: [
                                { id: "createdDate", displayName: "Start Date", type: "DateRange", selectedValue: start_date != null ? start_date.toISOString() : null },
                                { id: "endDate", displayName: "End Date", type: "DateRange", selectedValue: end_date != null ? end_date.toISOString() : null }
                            ],
                            displayedColumns: ['position', 'name', 'unitPrice', 'createdTimestamp', 'symbol'],
                            pageSizeOptions: [5, 10, 20],
                            data: data
                        }

                        body = reportData;

                    }
                    else if (reportId == 3) {
                        // Profit report

                        let filterExpression = "#status = :statusd  and  #businessEntityId = :businessEntityIdd "; // Initial conditions
                        let expressionAttributeNames = {
                            "#status": "status",
                            "#businessEntityId": "businessEntityId"
                        };
                        let expressionAttributeValues = {
                            ":statusd": "COMPLETE",
                            ":businessEntityIdd": businessEntityId
                        };

                        let start_date = null;
                        let end_date = null;

                        if (event.queryStringParameters != null) {
                            console.log("Query String parameters:", event.queryStringParameters);

                            //Handle requests with query string parameters here.
                            if (event.queryStringParameters.createdDate != null) {
                                let createdDate = event.queryStringParameters.createdDate;
                                console.log("Created Date:", createdDate);

                                start_date = new Date(createdDate);
                                end_date = new Date(start_date.getTime() + 86400000); // + 1 day in ms
                            }

                            if (event.queryStringParameters.endDate != null) {
                                let endDatePar = event.queryStringParameters.endDate;
                                console.log("End Date:", endDatePar);
                                let temp_end_date = new Date(endDatePar);

                                if (end_date < temp_end_date) {
                                    end_date = temp_end_date;
                                }
                            }
                            //Append queryString parameters to the scan query.
                            filterExpression = filterExpression + "AND createdTimestamp between :start_date and :end_date ";
                            expressionAttributeValues = {
                                ":statusd": "COMPLETE",
                                ":start_date": start_date.toISOString(),
                                ":end_date": end_date.toISOString(),
                                ":businessEntityIdd": businessEntityId
                            }
                        }

                        var params = {
                            TableName: tableName,
                            FilterExpression: filterExpression,
                            ExpressionAttributeNames: expressionAttributeNames,
                            ExpressionAttributeValues: expressionAttributeValues,
                            ProjectionExpression: 'cartItems',
                        };

                        let rawOutput = await dynamo.scan(params).promise();
                        let data = [];

                        var dataSummery = new Map();

                        for (let i = 0; i < rawOutput.Items.length; i++) {

                            for (var j = 0; j < rawOutput.Items[i].cartItems.length; j++) {
                                let tempObj = rawOutput.Items[i].cartItems[j]

                                let stockReferenceId = tempObj.stockReferenceId;
                                let unitPrice = tempObj.unitPrice;

                                let objectKey = "" + stockReferenceId + unitPrice;

                                var stockSummery = dataSummery.get(objectKey);
                                if (stockSummery == null || stockSummery == undefined) {
                                    stockSummery = tempObj;
                                }
                                else {
                                    stockSummery.quantity = Number(stockSummery.quantity) + Number(tempObj.quantity);
                                    //   stockSummery.quantity += tempObj.quantity;
                                }

                                dataSummery.set(objectKey, stockSummery);

                                // data.push(stockSummery);
                            }
                        }
                        
                        //Load stock items to calculate profit/loss

                        var params = {
                            TableName: "STOCK_ITEM",
                            FilterExpression: " #businessEntityId = :businessEntityIdd ",
                            ExpressionAttributeNames: {
                            "#businessEntityId": "businessEntityId"
                        },
                            ExpressionAttributeValues: {
                           ":businessEntityIdd": businessEntityId
                        },
                        ProjectionExpression: 'id, originalCost'
                        };

                        let rawStockItemsOutput = await dynamo.scan(params).promise();
                        console.log("rawStockItemsOutput" + JSON.stringify(rawStockItemsOutput, (key, value) => {
                                return (typeof value === 'bigint') ? value.toString() : value
                                }));
                                
                        let stockItems = rawStockItemsOutput.Items; // should not be empty

                        for (let [k, v] of dataSummery) {
                            
                            var stockItem = stockItems.find( x => x.id == v.stockReferenceId);
                            
                            // console.log("stockItem: " + JSON.stringify(stockItem, (key, value) => {
                            //     return (typeof value === 'bigint') ? value.toString() : value
                            //     }));
                            
                            // console.log("Key: " + k);
                            // console.log("Value: " + v);
                            let unitPrice = v.unitPrice;
                            let quantity = v.quantity;
                            let originalCost = v.markedPrice;
                            
                            
                            let netValue = (v.unitPrice - v.markedPrice) * v.quantity;
                            
                            console.log("unitPrice:["+unitPrice+"], originalCost:[], quantity:["+quantity+"], Net:["+netValue+"]");
                            
                            v["net"] = netValue ;
                            data.push(v);
                        }

                        let reportData = {
                            id: reportId,
                            displayName : "Profit Report",
                            columnDefinitions: [
                                { id: "position", type: "string", displayName: "ID", dataReference: "id" },
                                { id: "name", type: "string", displayName: "Name", dataReference: "name" },
                                { id: "unitPrice", type: "currency", displayName: "Price", dataReference: "unitPrice" },
                                { id: "net", type: "currency", displayName: "Net", dataReference: "net" },
                                // { id: "createdTimestamp", type: "date", displayName: "Created Date", dataReference: "createdTimestamp" },
                                { id: "symbol", type: "string", displayName: "Quantity", dataReference: "quantity" }
                            ],
                            filterDefinitions: [
                                { id: "createdDate", displayName: "Start Date", type: "DateRange", selectedValue: start_date != null ? start_date.toISOString() : null },
                                { id: "endDate", displayName: "End Date", type: "DateRange", selectedValue: end_date != null ? end_date.toISOString() : null }
                            ],
                            displayedColumns: ['position', 'name', 'unitPrice', 'createdTimestamp', 'symbol', 'net'],
                            pageSizeOptions: [5, 10, 20],
                            data: data
                        }

                        body = reportData;

                    }
                }
                else {
                    var params = {
                        TableName: tableName
                    };

                    let rawOutput = await dynamo.scan(params).promise();
                    body = rawOutput.Items;
                }

                break;
            case 'POST':
                console.log("Event Body:", event.body);

                var documentClient = new AWS.DynamoDB.DocumentClient();

                let UUID = generateRowId(4);
                console.log("UUID received:", UUID);

                var obj = JSON.parse(event.body);

                //Data enrichments
                obj.id = "" + generateRowId(4);
                obj.createdTimestamp = createdTimestamp;
                obj.lastUpdatedTimestamp = lastUpdatedTimestamp;

                if (obj.cartItems != null) {
                    for (var i = 0; i < obj.cartItems.length; i++) {

                        obj.cartItems[i].id = "" + generateRowId(4);
                        obj.cartItems[i].createdTimestamp = createdTimestamp;
                        obj.cartItems[i].lastUpdatedTimestamp = lastUpdatedTimestamp;

                        if (obj.cartItems[i].subCartItems != null) {
                            for (var j = 0; j < obj.cartItems[i].subCartItems.length; j++) {

                                obj.cartItems[i].subCartItems[j].id = "" + generateRowId(4);
                                obj.cartItems[i].subCartItems[j].createdTimestamp = createdTimestamp;
                                obj.cartItems[i].subCartItems[j].lastUpdatedTimestamp = lastUpdatedTimestamp;

                            }
                        }
                    }
                }

                await dynamo.put({
                    TableName: tableName,
                    Item: obj
                }).promise();

                body = obj;
                break;
            case 'PUT':
                var obj = JSON.parse(event.body);

                //Data enrichments
                obj.lastUpdatedTimestamp = lastUpdatedTimestamp;

                if (obj.cartItems != null) {
                    for (var i = 0; i < obj.cartItems.length; i++) {

                        obj.cartItems[i].id = "" + generateRowId(4);
                        obj.cartItems[i].lastUpdatedTimestamp = lastUpdatedTimestamp;

                        if (obj.cartItems[i] != null) {
                            if (obj.cartItems[i].subCartItems != null) {

                                for (var j = 0; j < obj.cartItems[i].subCartItems.length; j++) {

                                    obj.cartItems[i].subCartItems[j].id = "" + generateRowId(4);
                                    obj.cartItems[i].subCartItems[j].lastUpdatedTimestamp = lastUpdatedTimestamp;

                                }
                            }
                        }
                    }
                }
                body = await dynamo.put({
                    TableName: tableName,
                    Item: obj
                }).promise();
                break;
            case 'OPTIONS':
                body = "{}";
                break;
            default:
                throw new Error(`Unsupported method "${event.httpMethod}"`);
        }
    }
    catch (err) {
        statusCode = '400';
        body = err.message;
    }
    finally {
        body = JSON.stringify(body, (key, value) => {
            return (typeof value === 'bigint') ? value.toString() : value
        })
        body = JSON.parse(body);
        body = JSON.stringify(body);
    }

    console.log("Response to be returned - [body]:", body);
    console.log("Response to be returned - [statusCode]:", statusCode);
    console.log("Response to be returned - [headers]:", headers);

    return {
        statusCode,
        body,
        headers,
    };
};
