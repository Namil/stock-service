const AWS = require('aws-sdk');

// const uuid = require('uuid');

const dynamo = new AWS.DynamoDB.DocumentClient();

var CUSTOMEPOCH = 1300000000000; // artificial epoch
function generateRowId(shardId /* range 0-64 for shard/slot */) {
    var ts = new Date().getTime() - CUSTOMEPOCH; // limit to recent
    var randid = Math.floor(Math.random() * 512);
    ts = (ts * 64); // bit-shift << 6
    ts = ts + shardId;
    return (ts * 512) + (randid % 512);
}

class DataNotFoundInDatabaseError extends Error {
    constructor( description = 'Data Not found in database') {
      super(description);
    }
}


class Populator {
    apply(object) { }
}

class UUIDPopulator extends Populator {
    apply(object) {
        // console.log("UUID Populator")
        if (!object.id || object.id == null) {
            let id = generateRowId(4);
            object.id = id;
        }
    }
}

class RestHelper {

    constructor() {

    }

    getDBQueryParams(tableName, pathParameters, queryStringParameters) {

        console.log("Table: " + JSON.stringify(tableName));
        console.log("PathParam: " + JSON.stringify(pathParameters));
        console.log("QueryParam: " + JSON.stringify(queryStringParameters));

        let filterExpressions = [];
        let expressionAttributeNames = {};
        let expressionAttributeValues = {};
        let params = {};

        if (pathParameters && pathParameters != null) {
            Object.entries(pathParameters).forEach(([key, value]) => {

                if (key.endsWith('Id') || key.endsWith('id')) {
                    value = Number(value);
                }

                console.log("Path Parameters" + key + "," + value);
                filterExpressions.push("#" + key + " = :" + key + "d");

                let expAttrNameKey = "#" + key;
                expressionAttributeNames[expAttrNameKey] = key;

                let expAttrValueKey = ":" + key + "d";
                expressionAttributeValues[expAttrValueKey] = value;
            });
        }

        if (queryStringParameters) {
            Object.entries(queryStringParameters).forEach(([key, value]) => {
                console.log("QueryStringParameters: " + key + "," + value);

                if (key.endsWith('Id') || key.endsWith('id')) {
                    value = Number(value);
                }

                if (key === "filter") {
                    console.log("Filter query available: " + value);

                    //process filter query
                    let filterValue = value;
                    if (filterValue.startsWith("contains(", 0)) {
                        let queryData = filterValue.slice(filterValue.indexOf('(') + 1, filterValue.length - 1).split(",");
                        let queryColumnName = queryData[0];
                        let queryValues = queryData[1];

                        let expAttrNameKey = "#" + queryColumnName;
                        let expAttrValueKey = ":" + queryColumnName + "d";

                        filterExpressions.push("contains( " + expAttrNameKey + " , " + expAttrValueKey + " )");
                        expressionAttributeNames[expAttrNameKey] = queryColumnName;
                        expressionAttributeValues[expAttrValueKey] = "" + queryValues;
                    }
                } else {
                    let expAttrNameKey = "#" + key;
                    let expAttrValueKey = ":" + key + "d";

                    filterExpressions.push("" + expAttrNameKey + " = " + expAttrValueKey);
                    expressionAttributeNames[expAttrNameKey] = key;
                    expressionAttributeValues[expAttrValueKey] = value;
                }
            });
        }

        if (filterExpressions.length > 0) {

            let filterExpression = filterExpressions[0];
            for (let i = 1; i < filterExpressions.length; i++) {
                console.log("Filter expression availeble: " + filterExpressions[i] + " ");
                filterExpression = filterExpression + ' and ' + filterExpressions[i];
            }
            console.log("Filter expression: " + JSON.stringify(filterExpression));

            params = {
                TableName: tableName,
                FilterExpression: filterExpression,
                ExpressionAttributeNames: expressionAttributeNames,
                ExpressionAttributeValues: expressionAttributeValues
            };
        }
        else {
            params = {
                TableName: tableName
            };
        }

        console.log('ExpressionAttributeNames:', JSON.stringify(expressionAttributeNames, null, 2));
        console.log('ExpressionAttributeValues:', JSON.stringify(expressionAttributeValues, null, 2));
        console.log("Params: [" + JSON.stringify(params) + "]");

        return params;
    }
}

module.exports.RestHelper = RestHelper;

class TimestampPopulator extends Populator {
    apply(object) {
        // console.log("Timestamp Populator")
        var timestamp = new Date().toISOString();

        if (!object.createdTimestamp || object.createdTimestamp == null) {
            object.createdTimestamp = timestamp;
        }

        object.lastUpdatedTimestamp = timestamp;
    }
}

class CreateObjectValidator {
    apply(object) {
        if (!object && object != undefined) {
            throw new Error("Request object is NULL");
        }
    }
}


module.exports.getAll = (params, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    return new Promise(function (resolve, reject) {
        // Do async job
        dynamo.scan(params, function (error, result) {
            if (error) {
                console.log("getAll:" + error);
                reject(error);
            } else {

                console.log("getAll::Items retrieved from DB. Result size:[" + result.Items.length + "]");

                if (!result || !result.Items) {
                    reject( new Error('DB result is not valid'));
                }

                if (result.Items.length > 0) {
                    resolve(result.Items);
                }
                else {
                    console.log("Result has empty data - 404");
                    reject( new DataNotFoundInDatabaseError());
                }
            }
        })
    });
};

module.exports.getFirst = (params, context) => {
    context.callbackWaitsForEmptyEventLoop = false;

    return new Promise(function (resolve, reject) {
        // Do async job
        dynamo.scan(params, function (error, result) {
            if (error) {
                reject( error);
            } else {

                console.log("getFirst::Items retrieved from DB. Result size:[" + result.Items.length + "]");

                if (!result || !result.Items) {
                    reject(new Error( 'DB result is not valid' ));
                }

                if (result.Items.length > 0) {
                    resolve(result.Items[0]);
                }
                else {
                    console.log("Result has empty data - 404");
                    reject(new DataNotFoundInDatabaseError());
                }
            }
        })
    });
};

class DBHandler{

    constructor(dynamo){

    }

    saveData(tableName, object){
        let params = {
            TableName: tableName,
            Item: object
        };

        return new Promise(function (resolve, reject) {
            // Do async job
            dynamo.put(params, function (error, result) {
                if (error) {
                    reject(error);
                } else {
                    console.log("putDB::Items inserted/updated to DB.");
                    // Returning the same object which inserted to the DB
                    resolve(object);
                }
            })
        });
    }

}

module.exports.putDB = (object, context) => {
    context.callbackWaitsForEmptyEventLoop = false;
    let tableName = process.env.TABLE_NAME;

    let dbHandler = new DBHandler(dynamo);

    return dbHandler.saveData(tableName, object)

    // let tableName = process.env.TABLE_NAME;

    // var params = {
    //     TableName: tableName,
    //     Item: object
    // };

    // // let params = object;

    // return new Promise(function (resolve, reject) {
    //     // Do async job
    //     dynamo.put(params, function (error, result) {
    //         if (error) {
    //             reject({
    //                 statusCode: 500,
    //                 error: error
    //             });
    //         } else {
    //             console.log("putDB::Items inserted/updated to DB.");
    //             // Returning the same object which inserted to the DB
    //             resolve(object);
    //         }
    //     })
    // });
};

module.exports.populateFilterQueryFromPathParams = (pathParameters) => {

    return new Promise(function (resolve, reject) {

        let filterExpressions = [];
        let expressionAttributeNames = {};
        let expressionAttributeValues = {};
        let tableName = process.env.TABLE_NAME;
        let params = {};

        if (pathParameters && pathParameters != null) {
            for ([key, value] of Object.entries(pathParameters)) {

                if (key.endsWith('Id') || key.endsWith('id')) {
                    value = Number(value);
                }

                console.log("Path Parameters" + key + "," + value);
                filterExpressions.push("#" + key + " = :" + key + "d");

                let expAttrNameKey = "#" + key;
                expressionAttributeNames[expAttrNameKey] = key;

                let expAttrValueKey = ":" + key + "d";
                expressionAttributeValues[expAttrValueKey] = value;
            }

            let filterExpression = filterExpressions[0];
            for (i = 1; i < filterExpressions.length; i++) {
                console.log("Filter expression availeble: " + filterExpressions[i] + " ");
                filterExpression = filterExpression + ' and ' + filterExpressions[i];
            }
            console.log("Filter expression: " + JSON.stringify(filterExpression));

            params = {
                TableName: tableName,
                FilterExpression: filterExpression,
                ExpressionAttributeNames: expressionAttributeNames,
                ExpressionAttributeValues: expressionAttributeValues
            };
        }
        else {
            reject(new Error( "No Path Param available"));
        }

        console.log('ExpressionAttributeNames:', JSON.stringify(expressionAttributeNames, null, 2));
        console.log('ExpressionAttributeValues:', JSON.stringify(expressionAttributeValues, null, 2));

        resolve(params);
    });
}

module.exports.populateFilterQueryFromQueryParams = (queryStringParameters) => {

    return new Promise(function (resolve, reject) {

        let filterExpressions = [];
        let expressionAttributeNames = {};
        let expressionAttributeValues = {};
        let tableName = process.env.TABLE_NAME;
        let params = {};

        if (queryStringParameters) {
            for ([key, value] of Object.entries(queryStringParameters)) {
                console.log("QueryStringParameters: " + key + "," + value);

                if (key.endsWith('Id') || key.endsWith('id')) {
                    value = Number(value);
                }

                if (key === "filter") {
                    console.log("Filter query available: " + value);

                    //process filter query
                    let filterValue = value;
                    if (filterValue.startsWith("contains(", 0)) {
                        let queryData = filterValue.slice(filterValue.indexOf('(') + 1, filterValue.length - 1).split(",");
                        let queryColumnName = queryData[0];
                        let queryValues = queryData[1];

                        let expAttrNameKey = "#" + queryColumnName;
                        let expAttrValueKey = ":" + queryColumnName + "d";

                        filterExpressions.push("contains( " + expAttrNameKey + " , " + expAttrValueKey + " )");
                        expressionAttributeNames[expAttrNameKey] = queryColumnName;
                        expressionAttributeValues[expAttrValueKey] = "" + queryValues;
                    }
                } else {
                    let expAttrNameKey = "#" + key;
                    let expAttrValueKey = ":" + key + "d";

                    filterExpressions.push("" + expAttrNameKey + " = " + expAttrValueKey);
                    expressionAttributeNames[expAttrNameKey] = key;
                    expressionAttributeValues[expAttrValueKey] = value;
                }
            }

            let filterExpression = filterExpressions[0];
            for (i = 1; i < filterExpressions.length; i++) {
                // console.log("Filter expression availeble: " + filterExpressions[i] + " ");
                filterExpression = filterExpression + ' and ' + filterExpressions[i];
            }

            // console.log("Filter expression: " + JSON.stringify(filterExpression));

            params.TableName = tableName;
            params.FilterExpression = filterExpression;
            params.ExpressionAttributeNames = expressionAttributeNames;
            params.ExpressionAttributeValues = expressionAttributeValues;

        } else {
            params = {
                TableName: tableName
            };
        }

        console.log("Params: [" + JSON.stringify(params) + "]");
        resolve(params);
    });
}

module.exports.parseDBResult = (dbResult) => {
    return new Promise(function (resolve, reject) {

        try {
            result = JSON.stringify(dbResult, (key, value) => {
                return (typeof value === 'bigint') ? value.toString() : value
            })
            resolve(result);
        }
        catch (err) {
            console.log("Error [parseDBResult]" + err);
            reject(err);
        }
    });
}


module.exports.updateById = (params) => {
    context.callbackWaitsForEmptyEventLoop = false;

    return new Promise(function (resolve, reject) {
        // Do async job
        dynamo.put(params, function (error, result) {
            if (error) {
                reject(error);
            } else {
                resolve(result);
            }
        })
    });

};

module.exports.preprocessRequest = (obj, ...preprocessFunctions) => {
    return new Promise(function (resolve, reject) {

        console.log("Preprocessor functions:" + preprocessFunctions.length);

        try {
            preprocessFunctions.forEach(preprocessFunction => {
                preprocessFunction.apply(obj);
            });

            resolve(obj);
        }
        catch (exception) {
            console.log("Error: " + exception);
            reject(exception);
        }
    });

};

module.exports.preprocess = (obj, validatorFunctions, enrichmentFunctions = null) => {
    return new Promise(function (resolve, reject) {

        console.log("Preprocessor functions:" + validatorFunctions.length);

        try {
            validatorFunctions.forEach(validatorFunction => {
                validatorFunction.apply(obj);
            });

            if (enrichmentFunctions != null) {
                enrichmentFunctions.forEach(enrichmentFunction => {
                    enrichmentFunction.apply(obj);
                });
            }

            resolve(obj);
        }
        catch (exception) {
            console.log("Error: " + exception);
            reject(exception);
        }
    });

};

module.exports.postprocessRequest = (obj, ...preprocessFunctions) => {
    return new Promise(function (resolve, reject) {

        try {

            let result = obj;
            preprocessFunctions.forEach(preprocessFunction => {
                result = preprocessFunction.apply(result);
            });

            resolve(result);
        }
        catch (exception) {
            reject(exception);
        }
    });

};

module.exports.generateSuccessJsonResponseResult = (result, method = "GET") => {
    const headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": method,
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    };

    let response = {
        statusCode: 200,
        body: result,
        headers: headers
    }

    return response;
}

function generateErrorResponse(httpStatusCode, description, method){
    const headers = {
        'Content-Type': 'application/json',
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": method,
        "Access-Control-Allow-Headers": "Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token",
    };

    let response = {
        statusCode: httpStatusCode,
        body: JSON.stringify(description),
        headers: headers
    }

    return response;
};


module.exports.generateErrorJsonResponseResult = (exception, httpMethod) => {
        if(exception instanceof DataNotFoundInDatabaseError){
        return generateErrorResponse(404, exception.message, httpMethod);
    }
    else{
        return generateErrorResponse(500, exception.message,  httpMethod);
    }
};

// Populators
module.exports.POPULATOR_BASE = Populator;
module.exports.UUID_POPULATOR = UUIDPopulator;
module.exports.TIMESTAMP_POPULATOR = TimestampPopulator;

// Validators
module.exports.CREATE_OBJ_VALIDATOR = CreateObjectValidator;

// Enumerations
module.exports.HttpMethod = {
    GET: 'GET',
    PUT: 'PUT',
    POST: 'POST',
    DELETE: 'DELETE'
};